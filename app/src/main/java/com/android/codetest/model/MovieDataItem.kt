package com.android.codetest.model

import android.util.Log
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

private val CONST_POSTER_URL = "https://image.tmdb.org/t/p/w500/"

@JsonClass(generateAdapter = true)
data class MovieResponse(
    val page: Int,
    val total_results: Int,
    val total_pages: Int,
    val results: List<Movie>)

@JsonClass(generateAdapter = true)
@Entity
data class Movie(
    @PrimaryKey
    val id: Int,
    val popularity: Double,
    val video: Boolean,
    val vote_count: Int,
    val vote_average: Double,
    val title: String,
    val release_date: String?,
    val original_language: String,
    val original_title: String,
    val genre_ids: List<Int>,
    val backdrop_path: String?,
    val adult: Boolean,
    val overview: String,
    val poster_path: String?
) {
    // https://image.tmdb.org/t/p/w500/
    fun covertFullPosterPath(): String {
        Log.i("POSTER URL", CONST_POSTER_URL + poster_path)
        if(poster_path != null) {
            return if (!poster_path.contains(CONST_POSTER_URL)) CONST_POSTER_URL + poster_path else poster_path
        }
        return ""
    }
}

/*
"popularity": 0.84,
"id": 62516,
"video": false,
"vote_count": 2,
"vote_average": 3.5,
"title": "Bedmen",
"release_date": "1973-04-26",
"original_language": "tr",
"original_title": "Yarasa Adam - Betmen",
"genre_ids": [
878,
35
],
"backdrop_path": null,
"adult": false,
"overview": "From 1973, it's a Turkish Batman and Robin ignoring a Commissioner Gordon type before fighting a Blofeld-esque villain (bald, strokes cat) while spending their downtime in strip clubs! Yep, there's more nudity in this film than every other Batman movie combined, which admittedly isn't much but we'll take what we can get. For some reason the dynamic duo sometimes have capes and sometimes don't. In one scene Batman even picks up a gun and shoots a couple of bad guys before tucking the gun into his belt. You won't see that in the comics. This time no clips from other films (that I noticed anyway) but lots of music is lifted from elsewhere, including the James Bond theme.",
"poster_path": "/rYytyMvBK9NaLH9QvAP2WTEa3lX.jpg"
*/
