package com.android.codetest.network

import com.android.codetest.model.Movie
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

object MoshiAdapters{
    val moshi: Moshi = Moshi.Builder().build()

    private val movieDataObjectParameterized = Types.newParameterizedType(MutableList::class.java, Movie::class.java)
    val moviesDataObject: JsonAdapter<List<Movie>> = moshi.adapter<List<Movie>>(movieDataObjectParameterized)

    private val intDataObjectParameterized = Types.newParameterizedType(List::class.java, Int::class.java);
    val intDataObject: JsonAdapter<List<Int>> = moshi.adapter<List<Int>>(intDataObjectParameterized)

    private inline fun <reified T> adapter(): JsonAdapter<T>
            = moshi.adapter(T::class.java)

}