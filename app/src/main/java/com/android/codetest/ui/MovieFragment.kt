package com.android.codetest.ui

import android.app.SearchManager
import android.content.ContentResolver
import android.content.Context
import android.content.DialogInterface
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.codetest.R
import com.android.codetest.databinding.FragmentMovieBinding
import com.android.codetest.util.SearchRecentSuggestionsLimited
import com.android.codetest.util.SearchSuggestionAdapter
import com.android.codetest.util.SuggestionProvider
import com.android.codetest.viewmodels.MovieViewModel
import com.android.codetest.viewmodels.ViewModelFactory

class MovieFragment : Fragment() {

    private val viewModel: MovieViewModel by lazy {
        val activity = requireNotNull(this.activity)
        ViewModelProvider(this, ViewModelFactory(activity.application)).get(MovieViewModel::class.java)
    }

    private lateinit var binding: FragmentMovieBinding

    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter()
    }

    private lateinit var mSuggestionAdapter: SearchSuggestionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_movie,
                container,
                false)

        binding.lifecycleOwner = this
        binding.rvMovie.layoutManager = LinearLayoutManager(context)

        setupScrollListener()

        val query = viewModel.currentQueryData?: DEFAULT_QUERY

        viewModel.status.observe(viewLifecycleOwner, Observer {
            when(it.first){
                MovieViewModel.STATE_INITIAL ->{
                    binding.loadingSpinner.isVisible = true
                    Log.i("onCreateView", "STATE_INITIAL")
                    viewModel.searchMovie(query)
                }
                MovieViewModel.STATE_LOADING ->{
                    Log.i("onCreateView", "STATE_LOADING")
                    binding.loadingSpinner.isVisible = true
                    showEmptyList(false)
                }
                MovieViewModel.STATE_SUCCESS ->{
                    Log.i("onCreateView", "STATE_SUCCESS")
                    binding.loadingSpinner.isVisible = false
                }
                MovieViewModel.STATE_ERROR ->{
                    //check the error status and prompt
                    Log.i("onCreateView", "STATE_ERROR")
                    binding.loadingSpinner.isVisible = false
                    val error = it.second as Throwable
                    showAlert(getString(R.string.error_msg), error.localizedMessage?: getString(R.string.error_description))
                    showEmptyList(movieAdapter.itemCount == 0)
                }
            }
        })

        initAdapter()

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu.findItem(R.id.action_search)
        setupSearchView(searchItem)
    }

    private fun setupSearchView(searchItem: MenuItem) {
        val searchManager =
            activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
        mSuggestionAdapter = SearchSuggestionAdapter(
            requireContext(),
            null,
            0
        )
        searchView.suggestionsAdapter = mSuggestionAdapter
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("setupSearchView", "onQueryTextSubmit: $query")
                updateMovieListFromInput(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("setupSearchView","onQueryTextChange: $newText")
                val cursor = getRecentSuggestions(newText ?: "")
                Log.d("setupSearchView","onQueryTextChange: " + cursor!!.count)
                mSuggestionAdapter.swapCursor(cursor)
                return false
            }
        })

        searchView.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean {
                return false
            }

            override fun onSuggestionClick(position: Int): Boolean {
                val selectedQuery = mSuggestionAdapter.getSuggestionText(position)
                searchView.setQuery(selectedQuery, true)
                updateMovieListFromInput(selectedQuery)
                return true
            }
        })
    }

    fun getRecentSuggestions(query: String): Cursor? {
        val uriBuilder = Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(SuggestionProvider.AUTHORITY)
            .appendPath(SearchManager.SUGGEST_URI_PATH_QUERY)

        val selection = " ?"
        val selArgs = arrayOf(query)

        val uri = uriBuilder.build()
        return activity?.contentResolver?.query(uri, null, selection, selArgs,  null)
    }

    private fun initAdapter() {
        binding.rvMovie.adapter = movieAdapter
        viewModel.movies.observe(viewLifecycleOwner, Observer {
            movieAdapter.submitList(viewModel.movies.value)
            if (it.isEmpty()) {
                showEmptyList(true)
            } else {
                showEmptyList(false)
                // save suggestion only after search is valid
                val suggestions =
                    SearchRecentSuggestionsLimited(
                        requireContext(),
                        SuggestionProvider.AUTHORITY,
                        SuggestionProvider.MODE,
                        SuggestionProvider.LIMIT_COUNT
                    )
                suggestions.saveRecentQuery(viewModel.currentQueryData, null)
            }
        })
    }

    private fun updateMovieListFromInput(query: String?) {
        if (!query.isNullOrBlank()) {
            Log.i("updateMovieListFrmInput", "Query was submitted")
            binding.rvMovie.scrollToPosition(0)
            viewModel.searchMovie(query)
        }
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            binding.emptyList.visibility = View.VISIBLE
            binding.rvMovie.visibility = View.GONE
        } else {
            binding.emptyList.visibility = View.GONE
            binding.rvMovie.visibility = View.VISIBLE
        }
    }

    private fun setupScrollListener() {
        val layoutManager = binding.rvMovie.layoutManager as LinearLayoutManager
        binding.rvMovie.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }

    private fun showAlert(title: String, msg: String) {
        val alertDialogBuilder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(
            requireContext()
        )
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder
            .setMessage(msg)
            .setCancelable(false)
            .setNeutralButton("OK"

            ) { dialog, id -> }

        if (activity?.isFinishing == false) {
            alertDialogBuilder.create().show()
        }
    }

    companion object {
        private const val DEFAULT_QUERY = "Batman"
    }

}