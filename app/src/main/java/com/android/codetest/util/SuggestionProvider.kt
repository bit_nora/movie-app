package com.android.codetest.util

import android.content.SearchRecentSuggestionsProvider


class SuggestionProvider : SearchRecentSuggestionsProvider() {

    init {
        setupSuggestions(
            AUTHORITY,
            MODE
        )
    }

    companion object {
        const val AUTHORITY = "com.android.codetest.util.SuggestionProvider"
        const val MODE = DATABASE_MODE_QUERIES
        const val LIMIT_PARAMETER = "LIMIT"
        const val LIMIT_COUNT = 10
    }

}
