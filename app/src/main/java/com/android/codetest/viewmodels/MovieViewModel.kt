package com.android.codetest.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.android.codetest.BuildConfig
import com.android.codetest.model.Movie
import com.android.codetest.network.MovieRetrofitService
import kotlinx.coroutines.*

private const val STARTING_PAGE_INDEX = 1

class MovieViewModel(application: Application) : AndroidViewModel(application) {

    private val movieService = MovieRetrofitService.movieRestAdapter

    private var lastRequestedPage = STARTING_PAGE_INDEX
    private var isRequestInProgress = false

    private val inMemoryCache = mutableListOf<Movie>()
    var currentQueryData: String? = null

    private val _status = MutableLiveData<Pair<Int, Any?>>(Pair(STATE_INITIAL, null))
    val status: LiveData<Pair<Int, Any?>>
        get() = _status

    private val _movies = MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>>
        get() = _movies

    // Entry point for fetching movies
    fun searchMovie(query: String){
        _status.postValue(Pair(STATE_LOADING, null))

        currentQueryData = query
        val coroutineExceptionHandler = CoroutineExceptionHandler{_, e ->
            Log.e("searchMovie: ", e.localizedMessage.toString())
            _status.postValue(Pair(STATE_ERROR, e))
        }

        viewModelScope.launch (coroutineExceptionHandler + Dispatchers.IO){
            val movieList = fetchSearchResults(query)
            _status.postValue(Pair(STATE_SUCCESS, null))

            withContext(Dispatchers.Main) {
                _movies.value = movieList
            }
        }
    }

    // Fetch more results when user scrolled for paging behaviour
    fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        Log.i("listScrolled", "visibleItemCount: "+ visibleItemCount + ",lastVisibleItemPosition :" + lastVisibleItemPosition +", totalItemCount: "+ totalItemCount)
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount) {
            if (currentQueryData != null) {
                viewModelScope.launch(Dispatchers.IO) {
                    Log.i("loadMore", currentQueryData)
                    fetchMoreResults(currentQueryData!!)
                    val movieList = movieFilteredByName(currentQueryData!!)

                    withContext(Dispatchers.Main){
                        _movies.value = movieList
                    }
                }
            }
        }
    }

    // Fetch movie and save to local memory cache
    private suspend fun fetchSearchResults(query: String) : List<Movie>{
        lastRequestedPage = 1
        inMemoryCache.clear()
        if(fetchAndSaveMovieData(query)){
            lastRequestedPage++
        }
       return movieFilteredByName(query)
    }

    // Fetch movies from network
    private suspend fun fetchAndSaveMovieData(query: String): Boolean {
        isRequestInProgress = true
        val successful: Boolean

        val response = movieService.search(BuildConfig.MOVIE_API_KEY, query, lastRequestedPage)
        if(response.isSuccessful){
            Log.i("requestAndSaveData ", query +" "+ lastRequestedPage.toString()+ ", "+inMemoryCache.size.toString())
            val movies = response.body()?.results
            // Save to local memory cache
            if (movies != null) {
                inMemoryCache.addAll(movies)
            }
            Log.i("post inMemoryCache ", inMemoryCache.size.toString())
            successful = true
        }else{
            throw RuntimeException(response.errorBody()?.toString())
        }
        isRequestInProgress = false
        return successful
    }

    // Filter results from memory cache
    private fun movieFilteredByName(query: String): List<Movie> {
        return inMemoryCache.filter {
            it.original_title.contains(query, true)
        }
    }

    // Request more movies from server
    private suspend fun fetchMoreResults(query: String) {
        if (isRequestInProgress) return
        val successful = fetchAndSaveMovieData(query)
        if (successful) {
            lastRequestedPage++
        }
    }

    companion object {
        const val STATE_INITIAL = 1
        const val STATE_LOADING = 2
        const val STATE_SUCCESS = 3
        const val STATE_ERROR = 4
        private const val VISIBLE_THRESHOLD = 5
    }
}
